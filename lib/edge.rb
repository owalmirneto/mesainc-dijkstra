class Edge
  attr_accessor :from, :to, :cost

  def initialize(from, to, cost)
    @from = from
    @to = to
    @cost = cost
  end

  def <=>(other)
    cost <=> other.cost
  end

  def to_s
    "#{from} => #{to} with cost #{cost}"
  end
end
