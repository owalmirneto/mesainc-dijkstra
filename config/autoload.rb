class Autoload
  def self.call(folder = 'lib')
    new(folder).modules
  end

  def initialize(folder)
    @directory = "#{PATH}/#{folder}"
    @files = Dir.entries(directory) - ['.', '..']
  end

  def modules
    files.map do |file|
      filename = File.basename(file, File.extname(file))

      [camelize(filename).to_sym, "#{directory}/#{filename}"]
    end
  end

  private

  attr_accessor :directory, :files

  PATH = Dir.getwd

  def camelize(string, uppercase_first_letter = true)
    string =
      if uppercase_first_letter
        string.sub(/^[a-z\d]*/) { $&.capitalize }
      else
        string.sub(/^(?:(?=\b|[A-Z_])|\w)/) { $&.downcase }
      end
    string.gsub(/(?:_|(\/))([a-z\d]*)/) { "#{Regexp.last_match(1)}#{Regexp.last_match(2).capitalize}" }.gsub('/', '::')
  end
end

modules = Autoload.call('app') + Autoload.call
modules.map { |m| autoload m[0], m[1] }
