require 'csv'

class MountGraph
  attr_accessor :graph

  def initialize(file_path)
    @graph = Graph.new
    @rows = CSV.parse(File.read(file_path))
    @nodes = []
  end

  def load_nodes_and_edges
    mount_nodes
    mount_edges
  end

  def node_by_name(name)
    nodes.select { |n| n.name == name }.first
  end

  private

  attr_accessor :rows, :nodes

  def mount_nodes
    rows.each_with_index do |_costs, origin|
      @nodes << node = Node.new(origin)
      graph.add_node(node)
    end
  end

  def mount_edges
    rows.each_with_index do |costs, origin|
      closest = Float::INFINITY
      costs.each_with_index do |cost, destination|
        next if cost == 'x' && closest >= cost.to_i
        add_edge_to_graph(origin, destination, cost)
      end
    end
  end

  def add_edge_to_graph(origin, destination, cost)
    graph.add_edge(node_by_name(origin), node_by_name(destination), cost.to_i)
  end
end
