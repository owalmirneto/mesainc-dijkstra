# Dijkstra for Mesa

This is a test for [Mesa](http://mesainc.com.br) and was made with [Ruby 2.3.3](http://www.ruby-lang.org)

## Run

```bash
ruby app.rb [origin] [destination]
```

## Output

```bash
ruby app.rb
```

```
"Vertex: 2"
"Cost: 10"
"Path: 0 ~> 1 ~> 3"
```

```bash
ruby app.rb 4 6
```

```
"Vertex: 1"
"Cost: 5"
"Path: 4 ~> 6"
```
