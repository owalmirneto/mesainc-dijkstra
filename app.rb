require_relative 'config/autoload'

mounter = MountGraph.new('config/graph.csv')
mounter.load_nodes_and_edges

arg_origin = ARGV[0].nil? ? 0 : ARGV[0].to_i
arg_destination = ARGV[1].nil? ? 3 : ARGV[1].to_i

origin = mounter.node_by_name(arg_origin)
destination = mounter.node_by_name(arg_destination)

dijkstra = Dijkstra.new(mounter.graph, origin)
dijkstra.shortest_path_to(destination)

p "Vertex: #{dijkstra.vertex_count}"

p "Cost: #{dijkstra.cost_spent}"

p "Path: #{dijkstra.shortest_path}"
